## Is this a triangle?

Implement a method that accepts 3 integer values a, b, c. The method should return true if a triangle can be built with the sides of given length and false in any other case.

(In this case, all triangles must have surface greater than 0 to be accepted).

```go
func IsTriangle(a, b, c int) bool { 
    if (a == 0) || (b == 0) || (c == 0 ) {
        return  false; 
    } if (a + b > c) && (b+c > a) && (a+c > b) { 
        return  true; 
    } else { 
        return  false; 
    } 
}
```


## Fix string case

 I'm given a string that may have mixed uppercase and lowercase letters and your task is to convert that string to either lowercase only or uppercase only based on:

- make as few changes as possible.
- if the string contains equal number of uppercase and lowercase letters, convert the string to lowercase.

### For example:

- solve(`"coDe"`) = `"code"`. Lowercase characters > uppercase. Change only the `"D"` to lowercase. 
- solve(`"CODe"`) = `"CODE"`. Uppercase characters > lowecase. Change only the `"e"` to uppercase. 
- solve(`"coDE"`) = `"code"`. Upper == lowercase. Change all to lowercase.


```go
package kata 

import ( 
"regexp" 
"strings" 
)
 
func solve(str string) string { 

	//regex for lowercase letters 
	rel := regexp.MustCompile(`[a-z]`); 

	//regex for uppercase letters 
	reu := regexp.MustCompile(`[A-Z]`); 
	
	if(len(rel.FindAll([]byte(str), -1)) >= len(reu.FindAll([]byte(str), -1))) { 
		
		return strings.ToLower(str); 
	
	} else { 
		return strings.ToUpper(str); 
	
	} 
}
```
