## Vowel count

Return the number (count) of vowels in the given string.

We will consider a, e, i, o, and u as vowels for this Kata.

The input string will only consist of lower case letters and/or spaces.

`getCount :: String -> Int`\
`getCount xs = foldl (\acc x -> if (elem x ['a', 'e', 'i', 'o', 'u']) then  1 + acc else acc) 0 xs`



## findOdd

Given an array, find the int that appears an odd number of times.

There will always be only one integer that appears an odd number of times.


`findOdd :: [Int] -> Int`\
`findOdd xs = foldl(\acc x -> if odd $ length $ filter(==x) xs then x else acc) 0 xs`
