## Exclusive "or" (xor) Logical Operator


The **xor** function should have the behaviour the logical operator that it's named after, returning true if exactly one of the two expressions evaluate to true, false otherwise.

```c++
bool xorf(bool a, bool b) { 
    return a^b; 
}
```



## Duplicate Encoder

The goal of this exercise is to convert a string to a new string where each character in the new string is `"("` if that character appears only once in the original string, or `")"` if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.

### Example
    
- `"din"`      =>  `"((("`
- `"recede"`   =>  `"()()()"`
- `"Success"`  =>  `")())())"`
- `"(( @"`     =>  `"))(("`




```c++
#include <string>  
#include <algorithm>  
#include <cctype> 
using  namespace  std; 

std::string duplicate_encoder(const std::string& word){
    string encoded = ""; 
    string s = word; 
    transform(s.begin(), s.end(), s.begin(), ::tolower); 

    for(char c : s) {
        if ( count(s.begin(), s.end(), c) != 1 ) {
                encoded = encoded + ")";
        } else { 
            encoded = encoded + "("; 
        }
    } return encoded; 
}
```
